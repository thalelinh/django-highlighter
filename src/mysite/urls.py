"""mysite URL Configuration"""
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('highlighter/', include('highlighter.urls')),
    path('admin/', admin.site.urls),
]

